module.exports = {
  lintOnSave: false,
  devServer: {
    open: true
  },
  publicPath: './',
  css: {
    loaderOptions: {
      postcss: {
        plugins: [
          require('postcss-px2rem')({ remUnit: 192 }) // 换算的基数
        ]
      }
    }
  }
}
