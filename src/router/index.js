import Vue from 'vue'
import VueRouter from 'vue-router'
import Line from '../components/Line.vue'
import Index from '../view/Index.vue'

Vue.use(VueRouter)

const router = new VueRouter({
  routes: [
    { path: '/', component: Index, name: 'index' },
    { path: '/line', component: Line, name: 'line' }
  ]
})

export default router
