import Vue from 'vue'
import App from './App.vue'
import store from './store/index'
import router from './router/index'
import echarts from 'echarts'
import 'echarts-gl'
import './assets/js/china'
import './assets/js/world'
import './assets/js/lodash'

import 'lib-flexible'

Vue.config.productionTip = false

Vue.prototype.$echarts = echarts

new Vue({
  store,
  router,
  render: function (createElement) {
    return createElement(App)
  }
}).$mount('#app')
