const gauge = {
  state: {
    option: {
      tooltip: {
        formatter: '{a} <br/>{b} : {c}%'
      },
      radius: '80%',
      toolbox: {
        feature: {
          restore: {},
          saveAsImage: {}
        }
      },

      series: [
        {
          name: '业务指标',
          type: 'gauge',
          radius: '90%',
          detail: { formatter: '{value}%', color: '#fff', fontSize: 15 },
          data: [{ value: 50, name: '完成率' }],
          textStyle: {
            color: '#fff'
          },
          splitLine: {
            length: 30
          },
          title: {
            color: '#fff'
          },
          axisTick: {
            length: 10
          },

          splitNumber: 5
        }
      ]
    }
  },
  getters: {

  },
  mutations: {
    setSeries: function (state, payload) {
      state.option.series[0].data[0].value = payload
    }
  },
  actions: {
    setSeries: function (context, payload) {
      setInterval(function () {
        const r = (Math.random() * 100).toFixed(2) - 0
        context.commit('setSeries', r)
        // console.log('--r--action:', r)
      }, 1000)
    }
  }

}

export default gauge
