const placeHolderStyle = {
  normal: {
    label: {
      show: false
    },
    labelLine: {
      show: false
    },
    color: 'rgba(0,0,0,0)',
    borderWidth: 0
  },
  emphasis: {
    color: '#9FC6AF',
    borderWidth: 0
  }
}

const dataStyle = {
  normal: {
    formatter: '{c}%',
    position: 'center',
    show: true,
    textStyle: {
      fontSize: '20',
      fontWeight: 'normal',
      color: '#fff'
    }
  }
}

const comPie = {
  state: {
    option: {
      backgroundColor: 'transparent',
      title: [
        {
          text: '正面',
          left: '20%',
          top: '80%',
          textAlign: 'center',
          textStyle: {
            fontWeight: 'normal',
            fontSize: '16',
            color: '#fff',
            textAlign: 'center'
          }
        },
        {
          text: '负面',
          left: '80%',
          top: '80%',
          textAlign: 'center',
          textStyle: {
            color: '#fff',
            fontWeight: 'normal',
            fontSize: '16',
            textAlign: 'center'
          }
        }
      ],

      // 第一个图表
      series: [
        {
          type: 'pie',
          hoverAnimation: true, // 鼠标经过的特效
          radius: ['50%', '80%'],
          center: ['25%', '50%'],
          startAngle: 225,
          labelLine: {
            normal: {
              show: false
            }
          },
          label: {
            normal: {
              position: 'center'
            }
          },
          data: [
            {
              value: 100,
              itemStyle: {
                normal: {
                  color: '#E1E8EE'
                }
              }
            },
            {
              value: 35,
              itemStyle: placeHolderStyle
            }
          ]
        },
        // 上层环形配置
        {
          type: 'pie',
          hoverAnimation: false, // 鼠标经过的特效
          radius: ['50%', '80%'],
          center: ['25%', '50%'],
          startAngle: 225,
          labelLine: {
            normal: {
              show: false
            }
          },
          label: {
            normal: {
              position: 'center'
            }
          },
          data: [
            {
              value: 75,
              itemStyle: {
                normal: {
                  color: '#ffff99'
                }
              },
              label: dataStyle
            },
            {
              value: 35,
              itemStyle: placeHolderStyle
            }
          ]
        },

        // 第二个图表
        {
          type: 'pie',
          hoverAnimation: false,
          radius: ['50%', '80%'],
          center: ['75%', '50%'],
          startAngle: 225,
          labelLine: {
            normal: {
              show: false
            }
          },
          label: {
            normal: {
              position: 'center'
            }
          },
          data: [
            {
              value: 100,
              itemStyle: {
                normal: {
                  color: '#E1E8EE'
                }
              }
            },
            {
              value: 35,
              itemStyle: placeHolderStyle
            }
          ]
        },

        // 上层环形配置
        {
          type: 'pie',
          hoverAnimation: false,
          radius: ['50%', '80%'],
          center: ['75%', '50%'],
          startAngle: 225,
          labelLine: {
            normal: {
              show: false
            }
          },
          label: {
            normal: {
              position: 'center'
            }
          },
          data: [
            {
              value: 30,
              itemStyle: {
                normal: {
                  color: '#daa520'
                }
              },
              label: dataStyle
            },
            {
              value: 55,
              itemStyle: placeHolderStyle
            }
          ]
        }
      ]
    }
  },
  mutaions: {

  },
  actions: {

  },
  modules: {

  }
}

export default comPie
