const comLine = {
  namespaced: true,
  state: {
    lineData: {
      textStyle: {
        color: '#fff'
      },
      title: {
        text: '上半年增量',
        fontSize: 18,
        textStyle: {
          color: '#fff'
        }
      },
      tooltip: {},
      toolbox: {
        show: true,
        iconStyle: {
          // color: '#fff'
          borderColor: '#fff'
        },
        feature: {
          saveAsImage: {
            backgroundColor: '#081b4f'
          },
          restore: {},
          dataZoom: {},
          magicType: {
            type: ['line', 'bar']
          }
          // 选区
          // brush: { rect: true, polygon: true, lineX: true, lineY: true, keep: true, clear: true }
        }
      },
      legend: {
        data: ['销量'],
        textStyle: {
          color: '#fff'
        }
      },
      xAxis: {
        data: ['1月', '2月', '3月', '4月', '5月', '6月']
      },
      yAxis: {},
      series: [
        {
          name: '销量',
          type: 'bar',
          data: [5, 20, 36, 10, 10, 20]
        }
      ]
    }
  },
  mutations: {
    setLineData (state, payload) {
      console.log('state:', state.lineData)
      console.log('payLoad-mutations', payload)
      state.LineData = Object.assign(state.lineData, payload)
      // state.LineData.series[0].data = payload
    }
  },
  actions: {
    setLineData (context, payload) {
      setInterval(function () {
        console.log("zzy")
        state.LineData.series[0].data = [5, 20, 36, 10, 10, 20];
        state.LineData.series[0].data.sort(function(){
         return 0.5 - Math.random();
        })
        context.commit('setLineData', payload)
      }, 1000)
      // context.commit('setLineData', payload)
    }
  },
  getters: {}
}

export default comLine
