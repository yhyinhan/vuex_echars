import Vue from 'vue'
import Vuex from 'vuex'
import comLine from './comLine.js'
import comPie from './comPie.js'
import comAir from './airLine.js'
import comKLine from './kLine'
import comRadar from './radar'
import comEarch from './earth'
import comGauge from './gauge'
import treeMap from './treeMap'

Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    comLine,
    comPie,
    comAir,
    comKLine,
    comRadar,
    comEarch,
    comGauge,
    treeMap
  }
})

export default store
