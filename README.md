# test-echarts
这是一个学习echars的示例，采用flex布局适配pc和移动端

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).



效果图二维码：

![](./code.png)